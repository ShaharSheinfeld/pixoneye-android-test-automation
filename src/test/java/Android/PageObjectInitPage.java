package Android;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Android.AndroidHelper.*;

public class PageObjectInitPage
{
    AndroidDriver driver;
    WebDriverWait wait;

    public PageObjectInitPage(AndroidDriver driver, WebDriverWait wait)
    {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    @AndroidFindBy(id= BUTTON_START_INIT_SERVICE)
    @CacheLookup
    static WebElement buttonStartInitService;


    @AndroidFindBy(id = BUTTON_CLEAR_LOGS)
    @CacheLookup
    static WebElement buttonClearLogs;

    @AndroidFindBy(id = IMAGE_LOG_ID)
    @CacheLookup
    static WebElement image_log_id;



    public void clickStartInit(int initIterations, String waitForText)
    {
        for (int i = 0; i < initIterations; i++)
        {
            buttonStartInitService.click();
        }
        if (!waitForText.isEmpty())
        {
            wait.until(ExpectedConditions.textToBePresentInElement(image_log_id, waitForText));
        }
    }


    public void clickClearLogs()
    {
        buttonClearLogs.click();
    }

}
