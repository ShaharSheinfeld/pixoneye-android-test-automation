package Android;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;

class MyDesiredCapabilities
{
    static DesiredCapabilities setDesiredCapabilities() throws MalformedURLException
    {
        DesiredCapabilities capabilities= desiredCapabilities(60);
        return capabilities;
    }


    static DesiredCapabilities setDesiredCapabilities(int newCommandTimeOutCapability) throws MalformedURLException
    {
        DesiredCapabilities capabilities= desiredCapabilities(newCommandTimeOutCapability);
        return capabilities;
    }


    static DesiredCapabilities desiredCapabilities(int newCommandTimeOutCapability) throws MalformedURLException
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Galaxy S6");
        capabilities.setCapability("app", "C:\\WorkSpace\\BitBucket\\pixoneye-android-sdk\\SDK\\pixoneyesdktester\\build\\outputs\\apk\\qa\\debug\\pixoneyesdktester-qa-debug.apk");
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, newCommandTimeOutCapability);
        capabilities.setCapability("autoGrantPermissions", true);

        return capabilities;
    }
}
