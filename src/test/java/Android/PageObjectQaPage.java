package Android;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Android.AndroidHelper.*;

public class PageObjectQaPage
{
    AndroidDriver driver;
    WebDriverWait wait;

    public PageObjectQaPage(AndroidDriver driver, WebDriverWait wait)
    {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    @AndroidFindBy(id = IMAGE_MANAGER_LOGS)
    @CacheLookup
    WebElement image_manager_logs;

    @AndroidFindBy(id = BUTTON_CLEAR_DB)
    @CacheLookup
    static WebElement button_clear_db;

    @AndroidFindBy(id = BUTTON_DELETE_ALL_IMAGES)
    @CacheLookup
    static WebElement button_delete_all_images;


    public void clickButtonCopyImagesAmount(int imagesToCopy)
    {
        driver.findElement(By.id(COPY_IMAGES + imagesToCopy)).click();
        wait.until(ExpectedConditions.textToBePresentInElement(image_manager_logs, "Copied " + imagesToCopy +
                " images")); //wait until images are copied (i read the log on screen)
    }

    public void clickButtonClearDb()
    {
        button_clear_db.click();
        wait.until(ExpectedConditions.textToBePresentInElement(image_manager_logs, "Database cleared.")); //wait until DB cleared from images (i read the log on screen)
    }

    public void clickButtonDeleteAll()
    {
        button_delete_all_images.click();
        wait.until(ExpectedConditions.textToBePresentInElement(image_manager_logs, "Image folder deleted, image " +
                "removed from DB")); //wait until DB cleared from images (i read the log on screen)
    }

}
