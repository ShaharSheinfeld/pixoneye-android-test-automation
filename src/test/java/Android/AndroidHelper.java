package Android;

import com.google.gson.Gson;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.Connection;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.restassured.response.Response;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shahar on 7/2/17.
 */
class AndroidHelper
{

    final static String APP_ID = "20096d58-beea-4ce7-a3f9-8f263e0375d5";
    final static String API_KEY =
 "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJRCI6IjIwMDk2ZDU4LWJlZWEtNGNlNy1hM2Y5LThmMjYzZTAzNzVkNSIsInJvbGUiOiJxYSIsImlhdCI6MTUxMDgyOTA0MH0.vscnFDKd2oOBJQAWi2B_AvD43PxHsm4tO5K28KEvB5c";
    final static String ENABLE_POWER = "adb shell dumpsys battery set status 2 adb shell dumpsys battery set ac 1";
    final static String DISABLE_POWER = "adb shell dumpsys battery set status 4 adb shell dumpsys battery set usb 0 "
            + "adb shell dumpsys battery set ac 0";
    final static String RESET_POWER = "adb shell dumpsys battery reset";
    final static String POWER_USB = "adb shell dumpsys battery set status 4 adb shell dumpsys battery set usb 0 adb "
            + "shell dumpsys battery set ac 0 adb shell dumpsys  battery set status 3  adb  shell  dumpsys battery "
            + "set " + "usb 1";
    // Buttons
    final static String SDK_VERION_ID = "/hierarchy/android.widget.FrameLayout/android.widget" +
            ".LinearLayout/android" + ".widget.FrameLayout/android.view.ViewGroup/android.widget" +
             ".FrameLayout[2]/android.widget.LinearLayout/android.widget" + ".ScrollView/android.widget" +
            "" + ".LinearLayout/android.widget" + ".LinearLayout/android.widget.TextView[1]";
    final static String IMAGE_MANAGER_ID = "com.pixoneye.pixoneyesdktester:id/main_image_manager";

    final static String TITLE_ID = "com.pixoneye.pixoneyesdktester:id/screen_title";
//    final static String USB_DISCONNECT = "adb  shell  dumpsys battery set usb 0";

    //    final static String USB_CONNECT = "adb  shell  dumpsys battery set usb 1";
    final static String BUTTON_INITIALIZE_SERVICE_TESTER = "com.pixoneye.pixoneyesdktester:id/main_initialize_service";
    final static String COPY_IMAGES = "com.pixoneye.pixoneyesdktester:id/image_manager_copy_";
    final static String BUTTON_QA_PAGE = "com.pixoneye.pixoneyesdktester:id/main_qa_page";
    final static String BUTTON_DEVICE_INFO = "com.pixoneye.pixoneyesdktester:id/main_device_information";
    final static String BUTTON_DELETE_ALL_IMAGES = "com.pixoneye.pixoneyesdktester:id/image_manager_delete_all_images";
    final static String IMAGE_MANAGER_LOGS = "com.pixoneye.pixoneyesdktester:id/image_manager_status";
    final static String BUTTON_CLEAR_DB = "com.pixoneye.pixoneyesdktester:id/image_manager_clear_db";
    final static String DEVICE_ID = "com.pixoneye.pixoneyesdktester:id/deviceinfo_deviceid";
    final static String IMAGE_LOG_ID = "com.pixoneye.pixoneyesdktester:id/log_text_view";
    final static String BUTTON_CLEAR_LOGS = "com.pixoneye.pixoneyesdktester:id/initialize_service_clear_logs";
    final static String BUTTON_START_INIT_SERVICE = "com.pixoneye.pixoneyesdktester:id/initialize_service_start_service";
    final static String MAIN = "Main Screen";
    final static String INITIALIZE_SERVICE_TESTER = "Initialize service Tester";
    final static String IMAGE_MANAGER = "Every Day I'm Testing";
    final static String DEVICE_INFO = "Device Info";
    final static String adbPath = "/home/shahar/Android/Sdk/platform-tools/";
    final static String SCAN_COMPLETED = "Scan Completed";
    public static final String PIXONEYE_API = "https://api-qa.pixoneye.com/v1/";

    AndroidDriver driver;
    WebDriverWait wait;

    //methods


    @AndroidFindBy(id = IMAGE_MANAGER_LOGS)
    @CacheLookup
    WebElement image_manager_logs;

    @AndroidFindBy(id = TITLE_ID)
    public WebElement screenTitle;


    public AndroidHelper(AndroidDriver driver, WebDriverWait wait)
    {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    static void killApp() throws InterruptedException, IOException
    {
        System.out.println("Method: killApp");
        System.out.println("time of killing app with adb command: " + getTime());
        Runtime.getRuntime().exec("adb shell am kill com.pixoneye.pixoneyesdk.pixoneyesdktester").waitFor();
    }

    static void disableDevicePower() throws IOException, InterruptedException
    {
        System.out.println("Method: disableDevicePower");
        System.out.println("time of disable with adb command: " + getTime());
        Runtime.getRuntime().exec("adb shell dumpsys battery set status 4").waitFor();
        Runtime.getRuntime().exec("adb shell dumpsys battery set usb 0").waitFor();
        Runtime.getRuntime().exec("adb shell dumpsys battery set ac 0").waitFor();
    }

    static void enableDevicePower() throws IOException, InterruptedException
    {
        System.out.println("Method: enableDevicePower");
        System.out.println("time of enable with adb command: " + getTime());
        Runtime.getRuntime().exec("adb shell dumpsys battery set status 2").waitFor();
        Runtime.getRuntime().exec("adb shell dumpsys battery set ac 1").waitFor();
//        Runtime.getRuntime().exec("adb shell dumpsys battery reset").waitFor();
    }

    static int getImagesCountAPI(String deviceID)
    {
        System.out.println("Method: getImagesCountAndCompareExpacted");
        Response res = (Response) given().
                relaxedHTTPSValidation().
                header("api_key", API_KEY).
                get(PIXONEYE_API + "images/scanned-images-by-device-id/" + deviceID).
                then().
                extract();

        Gson gson = new Gson();
        ImagesCountResult imagesCountResult = gson.fromJson(res.asString(), ImagesCountResult.class);
        int count = imagesCountResult.getCount();
        return count;
    }

    static int waitForEsReceiveAllImages(String deviceID, int imagesCountExpected) throws InterruptedException
    {
        System.out.println("Method: waitForEsReceiveAllImages");
        int waitIterrations = (imagesCountExpected * 20);
        int count = getImagesCountAndCompareExpacted(deviceID, imagesCountExpected, waitIterrations);
        return count;
    }

    static int getImagesCountAndCompareExpacted(String deviceID, int imagesCountExpected) throws InterruptedException
    {
        System.out.println("Method: getImagesCountAndCompareExpacted");
        int count = getImagesCountAndCompareExpacted(deviceID, imagesCountExpected, 10);
        return count;
    }

    static String getTime()
    {
        System.out.println("Method: getTime");
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        return ft.format(dNow).toString();
    }

    static int checkIfSomeImagesArrivedToDB(String deviceID, int imagesMoreThan, int waitIterrations) throws
            InterruptedException
    {
        System.out.println("Method: getImagesCountAndCompareExpacted");
        int count = 0;
        for (int counter = 0; counter < waitIterrations; counter++)
        {
            count = getImagesCountAPI(deviceID);
            System.out.println(getTime() + ": checkIfSomeImagesArrivedToDB: Images in ES: " + count);
            if (count > imagesMoreThan)
            {
                break;
            }
            else
            {
                Thread.sleep(1000);
            }
        }
        assertThat(count).isGreaterThan(imagesMoreThan);
        return count;
    }

    static int getImagesCountAndCompareExpacted(String deviceID, int imagesCountExpected, int waitIterrations) throws
            InterruptedException
    {
        System.out.println("Method: getImagesCountAndCompareExpacted");
        int count = 0;
        for (int counter = 0; counter < waitIterrations; counter++)
        {
            count = getImagesCountAPI(deviceID);
            System.out.println(getTime() + ": getImagesCountAndCompareExpacted: Images in ES: " + count);
            if (count != imagesCountExpected)
            {
                Thread.sleep(1000);
            }
            else
            {
                break;
            }
        }
        assertThat(count).isEqualTo(imagesCountExpected);
        return count;
    }

    static void deleteImagesFromES_API(String deviceID) throws IOException, InterruptedException
    {
        System.out.println("Method: deleteImagesFromES_API");
        int imageAmount = getImagesCountAPI(deviceID);
        if (imageAmount > 0)
        {
            given().relaxedHTTPSValidation().header("api_key", API_KEY).contentType(JSON).delete("https://api-qa" +
                    ".pixoneye.com/v1/images/delete-images-by-device/" + deviceID);
            getImagesCountAndCompareExpacted(deviceID, 0);
        }
    }

    static void runAdbCommand(String command) throws IOException
    {
        System.out.println("Method: runAdbCommand");
        final String[] cmd = {"/bin/sh", "-c", command};
//        final String[] cmd = {command};
        Runtime.getRuntime().exec(cmd);
    }

    public String getScreenTitle()
    {
        String currentScreenTitle = screenTitle.getText();
        System.out.println("Method: getScreenTitle :" + currentScreenTitle);
        return currentScreenTitle;
    }

    public void navigateTo(String screen)
    {
        System.out.println("Method: navigateTo :" + screen);
        String clickElement = null;
        String currentScreenTitleIs = getScreenTitle();
        if (!currentScreenTitleIs.equals(screen))
        {
            if (!currentScreenTitleIs.equals(MAIN))
            {
                navigateToAppHomeScreen();
            }
            switch (screen)
            {
                case INITIALIZE_SERVICE_TESTER:
                    clickElement = BUTTON_INITIALIZE_SERVICE_TESTER;
                    break;
                case IMAGE_MANAGER:
                    clickElement = BUTTON_QA_PAGE;
                    break;
                case DEVICE_INFO:
                    clickElement = BUTTON_DEVICE_INFO;
                    break;
            }
            driver.findElement(By.id(clickElement)).click();
            wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.id(TITLE_ID)), screen));
        }
    }

    void navigateToAppHomeScreen()
    {
        System.out.println("Method: navigateToAppHomeScreen");
        String currentTitle = getScreenTitle();
        for (int i = 0; i < 5; i++)
        {
            if (!currentTitle.equals(MAIN))
            {
                driver.pressKeyCode(AndroidKeyCode.BACK); //go screen back in app
                currentTitle = getScreenTitle();
                if (currentTitle.equals(MAIN))
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }

    WebElement getSdkVersion()
    {
        System.out.println("Method: getSdkVersion");
        return driver.findElement(By.xpath(SDK_VERION_ID));
    }

    String getDeviceID()
    {
        System.out.println("Method: getDeviceID");
        PageObjectDeviceInfo pageObjectDeviceInfo = new PageObjectDeviceInfo(driver, wait);
        navigateTo(DEVICE_INFO); //navigate to device info screen
        String deviceID = pageObjectDeviceInfo.getDeviceId(); //get deviceID
        System.out.println("Device_ID = " + deviceID);
        return deviceID;
    }

    void copyImagesToDevice(int imagesToCopy)
    {
        System.out.println("Method: copyImagesToDevice");
        PageObjectQaPage pageObjectQaPage = new PageObjectQaPage(driver, wait);
        navigateTo(IMAGE_MANAGER);
        pageObjectQaPage.clickButtonCopyImagesAmount(imagesToCopy);
    }

    void clearDevice() throws InterruptedException
    {
        System.out.println("Method: clearDevice");
        clearDb();
        deleteImagesFromDevice();
    }

    void deleteImagesFromDevice()
    {
        System.out.println("Method: deleteImagesFromDevice");
        PageObjectQaPage pageObjectQaPage = new PageObjectQaPage(driver,wait);
        navigateTo(IMAGE_MANAGER);
        pageObjectQaPage.clickButtonDeleteAll();
    }

    void clearDb()
    {
        System.out.println("Method: clearDevice");
        PageObjectQaPage pageObjectQaPage = new PageObjectQaPage(driver,wait);
        navigateTo(IMAGE_MANAGER);
        pageObjectQaPage.clickButtonClearDb();
    }

    void startInitServiceAndWaitForText(int amountOfInits, String waitForText)
    {
        System.out.println("Method: startInitServiceAndWaitForText");
        startInitService(amountOfInits, waitForText);
    }

    void startInitServiceAndWaitForText(String waitForText)
    {
        System.out.println("Method: startInitServiceAndWaitForText");
        startInitService(1, waitForText);
    }

    void startInitServiceWithoutWaitingForText()
    {
        System.out.println("Method: startInitServiceWithoutWaitingForText");
        startInitService(1, null);
    }

    private void startInitService(int initAmount, String waitForText)
    {
        System.out.println("Method: startInitService");
        PageObjectInitPage pageObjectInitPage = new PageObjectInitPage(driver, wait);
        navigateTo(INITIALIZE_SERVICE_TESTER);
        pageObjectInitPage.clickClearLogs();
        pageObjectInitPage.clickStartInit(initAmount, waitForText);
    }

    void goScreensBack(int screensBack) throws InterruptedException
    {
        System.out.println("Method: goScreensBack");
        for (int i = 0; i < screensBack; i++)
        {
            driver.pressKeyCode(AndroidKeyCode.BACK);
            Thread.sleep(1500);
        }
    }

    void runAppInBackgroundInSeconds(int seconds)
    {
        System.out.println("Method: runAppInBackgroundInSeconds");
        driver.runAppInBackground(Duration.ofSeconds(seconds));  //sends the app to the background
    }

    void setConnection(Connection connectionType)
    {
        System.out.println("Method: setConnection " + connectionType);
        Connection currentConnectionType = driver.getConnection();
        if (currentConnectionType != connectionType)
        {
            driver.setConnection(connectionType);
        }
        assertThat(driver.getConnection().equals(connectionType));
    }
}
