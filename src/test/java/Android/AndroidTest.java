package Android;

import com.google.gson.Gson;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.Connection;
import io.restassured.response.Response;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;

import static Android.AndroidHelper.*;
import static Android.MyDesiredCapabilities.setDesiredCapabilities;
import static io.restassured.RestAssured.given;

/**
 * Created by Shahar Sheinfeld on 21/06/2017.
 */
public class AndroidTest extends BaseClass
{
    WebDriverWait wait;
    AndroidDriver driver;
    AndroidHelper androidHelper;

//    @BeforeTest
//    public  void setUp() throws MalformedURLException
//    {
////        DesiredCapabilities capabilities = new DesiredCapabilities();
////        capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
////        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy S6");
////        capabilities.setCapability("platformVersion", "7.0");
////        capabilities.setCapability(MobileCapabilityType.APP, "C:\\1\\pixoneyesdktester-debug.apk");
////        capabilities.setCapability("autoGrantPermissions", true);
////        capabilities.setCapability("appPackage", "com.pixoneye.pixoneyesdk.pixoneyesdktester");
////        capabilities.setCapability("appActivity", ".InitFlowTesterActivity");
////        capabilities.setCapability("fullReset", true);  //Stop app, clear app data and uninstall apk after test
////        capabilities.setCapability("noReset", true);
//
//
//        //************works on mobile*************************
////        DesiredCapabilities capabilities = new DesiredCapabilities();
////        capabilities.setCapability("platformName", "Android");
////        capabilities.setCapability("deviceName", "Galaxy S6");
////        capabilities.setCapability("app", "/home/shahar/StudioProjects/pixoneye-android-sdk/SDK/pixoneyesdktester
// /build/outputs/apk/pixoneyesdktester-qa.apk");
////        capabilities.setCapability("autoAcceptAlerts", true);
/////////////////////////////////////////////
//
////*******************************works on emulator ***********************************
////        DesiredCapabilities capabilities = new DesiredCapabilities();
////        capabilities.setCapability("platformName", "Android");
////        capabilities.setCapability("deviceName", "Android Emulator");
////        capabilities.setCapability("platformVersion", "8.0.0");
////        capabilities.setCapability("app", "C:\\1\\pixoneyesdktester-debug.apk");
////        capabilities.setCapability("autoGrantPermissions", true);
////
////***************************************************************************************
//        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), setDesiredCapabilities());
////        wait = new WebDriverWait(driver, 60);
////        setConnection(driver, Connection.WIFI);
//    }

    //    @AfterTest
//    public void tearDown() throws Exception
//    {
//        try
//        {
//            driver.quit();
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.getMessage());
//        }
//    }

    @BeforeTest
    void beforeTest()
    {
        driver = getDriver();
        wait = getWait();
        androidHelper = new AndroidHelper(driver, wait);
    }


    @Test
    void triggerScanWhenAppIsKilled() throws InterruptedException, IOException
    {
        driver.quit();
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), setDesiredCapabilities(300));
        int imagesToCopy = 50;
        String deviceID = androidHelper.getDeviceID();
        deleteImagesFromES_API(deviceID);
        androidHelper.clearDevice();
        androidHelper.copyImagesToDevice(imagesToCopy);
        androidHelper.startInitServiceWithoutWaitingForText();
        androidHelper.goScreensBack(2);

        killApp();
        disableDevicePower();
        enableDevicePower();

        waitForEsReceiveAllImages(deviceID, imagesToCopy);
    }

    @Test
    public void scan2images() throws IOException, InterruptedException
    {
        String deviceID = androidHelper.getDeviceID();
        deleteImagesFromES_API(deviceID);
        androidHelper.clearDevice();
        androidHelper.copyImagesToDevice(2);
        androidHelper.startInitServiceAndWaitForText(SCAN_COMPLETED);
        getImagesCountAndCompareExpacted(deviceID, 2);
    }


    @Test //Doesn't work on samsung phones
    public void scan2imagesWithoutNetwork() throws IOException, InterruptedException
    {
        String deviceManufacturer = driver.getCapabilities().getCapability("deviceManufacturer").toString()
                .toLowerCase();
        if (!deviceManufacturer.equals("samsung"))
        {
            androidHelper.clearDevice();
            androidHelper.startInitServiceAndWaitForText(SCAN_COMPLETED);
            androidHelper.setConnection(Connection.AIRPLANE);
            String deviceID = androidHelper.getDeviceID();
            deleteImagesFromES_API(deviceID);
            androidHelper.copyImagesToDevice(2);
            androidHelper.startInitServiceAndWaitForText(SCAN_COMPLETED);
            getImagesCountAndCompareExpacted(deviceID, 0); //checks that 0 images arrived to ES
            androidHelper.setConnection(Connection.WIFI);
            androidHelper.runAppInBackgroundInSeconds(2);
            getImagesCountAndCompareExpacted(deviceID, 2); //checks that 2 images arrived to ES
        }
    }

    @Test
    public void runInitManyTimes() throws IOException, InterruptedException
    {
        String deviceID = androidHelper.getDeviceID();
        deleteImagesFromES_API(deviceID);
        androidHelper.clearDevice();
        androidHelper.copyImagesToDevice(10);
        androidHelper.startInitServiceAndWaitForText(10, SCAN_COMPLETED);
        getImagesCountAndCompareExpacted(deviceID, 10);
    }

    @Test
    public void runInitWithoutConfigurationModelAndNetwork() throws IOException, InterruptedException
    {
        androidHelper.clearDevice();
        androidHelper.copyImagesToDevice(2);
        androidHelper.setConnection(Connection.AIRPLANE);
        String deviceID = androidHelper.getDeviceID();
        deleteImagesFromES_API(deviceID);
        androidHelper.startInitServiceAndWaitForText("Disabled by configuration");
        getImagesCountAndCompareExpacted(deviceID, 0);
        androidHelper.setConnection(Connection.WIFI);
    }

    @Test
    public void deleteImagesDuringScan() throws IOException, InterruptedException
    {
        String deviceID = androidHelper.getDeviceID();
        deleteImagesFromES_API(deviceID);
        androidHelper.clearDevice();
        androidHelper.copyImagesToDevice(20);
        androidHelper.startInitServiceAndWaitForText("image scan complete");
        androidHelper.deleteImagesFromDevice();
        androidHelper.startInitServiceAndWaitForText("Scan Completed");
        checkIfSomeImagesArrivedToDB(deviceID, 0, 20);
    }

//    @Test
//    public void sdkDisabled()
//    {
//        // Need to do get-token to receive api_key to use in the get configuration
//
//        Response res = (Response) given().
//                relaxedHTTPSValidation().
//                header("api_key", API_KEY).
//                get(PIXONEYE_API + "config/" + APP_ID).
//                then().
//                extract();
//
//        String jsonStringFromPixoneyeServer = res.asString();
//        System.out.println(jsonStringFromPixoneyeServer);
//
//        Gson gson = new Gson();
//        GetConfigByAppId getConfigurationResponse = gson.fromJson(jsonStringFromPixoneyeServer, GetConfigByAppId.class);
////        Boolean isBoolean = getConfigurationResponse.getAndroidConfig().isSdkEnabled();
//    }



}