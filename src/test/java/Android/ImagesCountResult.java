package Android;

public class ImagesCountResult
{

    /**
     * count : 2
     */

    private int count;

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }
}
