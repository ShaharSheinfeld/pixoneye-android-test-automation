package Android;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

import static Android.MyDesiredCapabilities.setDesiredCapabilities;

class BaseClass
{

    private static AndroidDriver driver;
    private static WebDriverWait wait;

    static AndroidDriver getDriver()
    {
        return driver;
    }


    static WebDriverWait getWait()
    {
        return wait;
    }


    @BeforeTest
    void setupApplication() throws MalformedURLException
    {
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), setDesiredCapabilities());
        wait = new WebDriverWait(driver, 120);
    }


    @AfterTest
    void closeApplication()

    {
        driver.quit();
    }
}
