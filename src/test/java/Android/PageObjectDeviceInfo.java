package Android;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Android.AndroidHelper.DEVICE_ID;

public class PageObjectDeviceInfo
{
    AndroidDriver driver;
    WebDriverWait wait;

    public PageObjectDeviceInfo(AndroidDriver driver, WebDriverWait wait)
    {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    @AndroidFindBy(id= DEVICE_ID)
    @CacheLookup
    static WebElement device_id;


    public String getDeviceId()
    {
        String deviceId = device_id.getText();
        return deviceId;
    }
}
